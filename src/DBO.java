

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class DBO {

    Connection conn;
    Statement st;
    ResultSet rs;

    public DBO() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ooad", "root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getOrderID() {
        try {
            st = conn.createStatement();
            String str = "SELECT MAX(ORDER_ID) FROM orderid;";
            rs = st.executeQuery(str);
            rs.next();
            int id = rs.getInt(1) + 1;
            str = "insert into orderid values (" + id + ")";
            st.execute(str);
            return id;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }

    }

    public void addMenu(int itemCount, String name, String qntity, String price, int id) {
        try {

            st = conn.createStatement();
            String str = "insert into menuitems values (?,?,?,?,?);";
            PreparedStatement ps = conn.prepareStatement(str);
            ps.setInt(1, itemCount);
            ps.setString(2, name);
            ps.setString(3, qntity);
            ps.setString(4, price);
            ps.setInt(5, id);
            ps.execute();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResultSet getOrderDetails(int orderID) {
        try {
            st = conn.createStatement();
            String str = "Select Item_id as 'Item No', item_name as 'Item Name',item_quantity as 'Quantity', item_price as 'Price' from menuItems where order_Id =" + orderID + ";";
            rs = st.executeQuery(str);

            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    boolean userExists(String email, String password) {
        try {
            Statement s = conn.createStatement();
            String query = "SELECT COUNT(*) FROM USER WHERE EMAIL = '" + email + "';";
            rs = s.executeQuery(query);
            rs.next();
            return rs.getInt(1) != 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
