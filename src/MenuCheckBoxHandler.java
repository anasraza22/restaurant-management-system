

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class MenuCheckBoxHandler implements ActionListener {

    MenuItemsPannel items;
    DBO dbo;
    int count = 0;

    public MenuCheckBoxHandler(MenuItemsPannel items) {
        this.items = items;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == items.jb) {
            int count = 0;
            for (int i = 0; i < 67; i++) {
                if (items.box[i].isSelected()) {
                    count++;
                }
            }
            if (count > 0) {

                dbo = new DBO();
                count = 0;
                int id = dbo.getOrderID();
                for (int i = 0; i < 67; i++) {
                    if (items.box[i].isSelected()) {
                        if (!"0".equals(items.textField[i].getText())) {
                            count++;
                            dbo.addMenu(count, items.box[i].getText(), items.textField[i].getText(), items.lable[i].getText(), id);
                        }
                    }

                }
                try {
                    new Invoice(id, items);
                } catch (SQLException ex) {
                    Logger.getLogger(MenuCheckBoxHandler.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please Select any Item");
            }

        }
        if (items.box[0].isSelected()) {
            items.textField[0].setEnabled(true);
        } else {
            items.textField[0].setEnabled(false);
        }
        if (items.box[1].isSelected()) {
            items.textField[1].setEnabled(true);
        } else {
            items.textField[1].setEnabled(false);
        }
        if (items.box[2].isSelected()) {
            items.textField[2].setEnabled(true);
        } else {
            items.textField[2].setEnabled(false);
        }
        if (items.box[3].isSelected()) {
            items.textField[3].setEnabled(true);
        } else {
            items.textField[3].setEnabled(false);
        }
        if (items.box[4].isSelected()) {
            items.textField[4].setEnabled(true);
        } else {
            items.textField[4].setEnabled(false);
        }
        if (items.box[5].isSelected()) {
            items.textField[5].setEnabled(true);
        } else {
            items.textField[5].setEnabled(false);
        }
        if (items.box[6].isSelected()) {
            items.textField[6].setEnabled(true);
        } else {
            items.textField[6].setEnabled(false);
        }
        if (items.box[7].isSelected()) {
            items.textField[7].setEnabled(true);
        } else {
            items.textField[7].setEnabled(false);
        }
        if (items.box[8].isSelected()) {
            items.textField[8].setEnabled(true);
        } else {
            items.textField[8].setEnabled(false);
        }
        if (items.box[9].isSelected()) {
            items.textField[9].setEnabled(true);
        } else {
            items.textField[9].setEnabled(false);
        }
        if (items.box[10].isSelected()) {
            items.textField[10].setEnabled(true);
        } else {
            items.textField[10].setEnabled(false);
        }
        if (items.box[11].isSelected()) {
            items.textField[11].setEnabled(true);
        } else {
            items.textField[11].setEnabled(false);
        }
        if (items.box[12].isSelected()) {
            items.textField[12].setEnabled(true);
        } else {
            items.textField[12].setEnabled(false);
        }
        if (items.box[13].isSelected()) {
            items.textField[13].setEnabled(true);
        } else {
            items.textField[13].setEnabled(false);
        }
        if (items.box[14].isSelected()) {
            items.textField[14].setEnabled(true);
        } else {
            items.textField[14].setEnabled(false);
        }
        if (items.box[15].isSelected()) {
            items.textField[15].setEnabled(true);
        } else {
            items.textField[15].setEnabled(false);
        }
        if (items.box[16].isSelected()) {
            items.textField[16].setEnabled(true);
        } else {
            items.textField[16].setEnabled(false);
        }
        if (items.box[17].isSelected()) {
            items.textField[17].setEnabled(true);
        } else {
            items.textField[17].setEnabled(false);
        }
        if (items.box[18].isSelected()) {
            items.textField[18].setEnabled(true);
        } else {
            items.textField[18].setEnabled(false);
        }
        if (items.box[19].isSelected()) {
            items.textField[19].setEnabled(true);
        } else {
            items.textField[19].setEnabled(false);
        }
        if (items.box[20].isSelected()) {
            items.textField[20].setEnabled(true);
        } else {
            items.textField[20].setEnabled(false);
        }
        if (items.box[21].isSelected()) {
            items.textField[21].setEnabled(true);
        } else {
            items.textField[21].setEnabled(false);
        }
        if (items.box[22].isSelected()) {
            items.textField[22].setEnabled(true);
        } else {
            items.textField[22].setEnabled(false);
        }
        if (items.box[23].isSelected()) {
            items.textField[23].setEnabled(true);
        } else {
            items.textField[23].setEnabled(false);
        }
        if (items.box[24].isSelected()) {
            items.textField[24].setEnabled(true);
        } else {
            items.textField[24].setEnabled(false);
        }
        if (items.box[25].isSelected()) {
            items.textField[25].setEnabled(true);
        } else {
            items.textField[25].setEnabled(false);
        }
        if (items.box[26].isSelected()) {
            items.textField[26].setEnabled(true);
        } else {
            items.textField[26].setEnabled(false);
        }
        if (items.box[27].isSelected()) {
            items.textField[27].setEnabled(true);
        } else {
            items.textField[27].setEnabled(false);
        }
        if (items.box[28].isSelected()) {
            items.textField[28].setEnabled(true);
        } else {
            items.textField[28].setEnabled(false);
        }
        if (items.box[29].isSelected()) {
            items.textField[29].setEnabled(true);
        } else {
            items.textField[29].setEnabled(false);
        }
        if (items.box[30].isSelected()) {
            items.textField[30].setEnabled(true);
        } else {
            items.textField[30].setEnabled(false);
        }
        if (items.box[31].isSelected()) {
            items.textField[31].setEnabled(true);
        } else {
            items.textField[31].setEnabled(false);
        }
        if (items.box[32].isSelected()) {
            items.textField[32].setEnabled(true);
        } else {
            items.textField[32].setEnabled(false);
        }
        if (items.box[33].isSelected()) {
            items.textField[33].setEnabled(true);
        } else {
            items.textField[33].setEnabled(false);
        }
        if (items.box[34].isSelected()) {
            items.textField[34].setEnabled(true);
        } else {
            items.textField[34].setEnabled(false);
        }
        if (items.box[35].isSelected()) {
            items.textField[35].setEnabled(true);
        } else {
            items.textField[35].setEnabled(false);
        }
        if (items.box[36].isSelected()) {
            items.textField[36].setEnabled(true);
        } else {
            items.textField[36].setEnabled(false);
        }
        if (items.box[37].isSelected()) {
            items.textField[37].setEnabled(true);
        } else {
            items.textField[37].setEnabled(false);
        }
        if (items.box[38].isSelected()) {
            items.textField[38].setEnabled(true);
        } else {
            items.textField[38].setEnabled(false);
        }
        if (items.box[39].isSelected()) {
            items.textField[39].setEnabled(true);
        } else {
            items.textField[39].setEnabled(false);
        }
        if (items.box[40].isSelected()) {
            items.textField[40].setEnabled(true);
        } else {
            items.textField[40].setEnabled(false);
        }
        if (items.box[41].isSelected()) {
            items.textField[41].setEnabled(true);
        } else {
            items.textField[41].setEnabled(false);
        }
        if (items.box[42].isSelected()) {
            items.textField[42].setEnabled(true);
        } else {
            items.textField[42].setEnabled(false);
        }
        if (items.box[43].isSelected()) {
            items.textField[43].setEnabled(true);
        } else {
            items.textField[43].setEnabled(false);
        }
        if (items.box[44].isSelected()) {
            items.textField[44].setEnabled(true);
        } else {
            items.textField[44].setEnabled(false);
        }
        if (items.box[45].isSelected()) {
            items.textField[45].setEnabled(true);
        } else {
            items.textField[45].setEnabled(false);
        }
        if (items.box[46].isSelected()) {
            items.textField[46].setEnabled(true);
        } else {
            items.textField[46].setEnabled(false);
        }
        if (items.box[47].isSelected()) {
            items.textField[47].setEnabled(true);
        } else {
            items.textField[47].setEnabled(false);
        }
        if (items.box[48].isSelected()) {
            items.textField[48].setEnabled(true);
        } else {
            items.textField[48].setEnabled(false);
        }
        if (items.box[49].isSelected()) {
            items.textField[49].setEnabled(true);
        } else {
            items.textField[49].setEnabled(false);
        }
        if (items.box[50].isSelected()) {
            items.textField[50].setEnabled(true);
        } else {
            items.textField[50].setEnabled(false);
        }
        if (items.box[51].isSelected()) {
            items.textField[51].setEnabled(true);
        } else {
            items.textField[51].setEnabled(false);
        }
        if (items.box[52].isSelected()) {
            items.textField[52].setEnabled(true);
        } else {
            items.textField[52].setEnabled(false);
        }
        if (items.box[53].isSelected()) {
            items.textField[53].setEnabled(true);
        } else {
            items.textField[53].setEnabled(false);
        }
        if (items.box[54].isSelected()) {
            items.textField[54].setEnabled(true);
        } else {
            items.textField[54].setEnabled(false);
        }
        if (items.box[55].isSelected()) {
            items.textField[55].setEnabled(true);
        } else {
            items.textField[55].setEnabled(false);
        }
        if (items.box[56].isSelected()) {
            items.textField[56].setEnabled(true);
        } else {
            items.textField[56].setEnabled(false);
        }
        if (items.box[57].isSelected()) {
            items.textField[57].setEnabled(true);
        } else {
            items.textField[57].setEnabled(false);
        }
        if (items.box[58].isSelected()) {
            items.textField[58].setEnabled(true);
        } else {
            items.textField[58].setEnabled(false);
        }
        if (items.box[59].isSelected()) {
            items.textField[59].setEnabled(true);
        } else {
            items.textField[59].setEnabled(false);
        }
        if (items.box[60].isSelected()) {
            items.textField[60].setEnabled(true);
        } else {
            items.textField[60].setEnabled(false);
        }
        if (items.box[61].isSelected()) {
            items.textField[61].setEnabled(true);
        } else {
            items.textField[61].setEnabled(false);
        }
        if (items.box[62].isSelected()) {
            items.textField[62].setEnabled(true);
        } else {
            items.textField[62].setEnabled(false);
        }
        if (items.box[63].isSelected()) {
            items.textField[63].setEnabled(true);
        } else {
            items.textField[63].setEnabled(false);
        }
        if (items.box[64].isSelected()) {
            items.textField[64].setEnabled(true);
        } else {
            items.textField[64].setEnabled(false);
        }
        if (items.box[65].isSelected()) {
            items.textField[65].setEnabled(true);
        } else {
            items.textField[65].setEnabled(false);
        }
        if (items.box[66].isSelected()) {
            items.textField[66].setEnabled(true);
        } else {
            items.textField[66].setEnabled(false);
        }
    }

}
