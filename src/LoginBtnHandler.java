

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class LoginBtnHandler implements ActionListener {

    Login login;

    public LoginBtnHandler(Login log) {
        this.login = log;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == login.loginButton) {
            if ("admin".equals(login.nameField.getText()) && "admin".equals(login.passwordField.getText())) {
                login.jf.dispose();
                new WelcomeScreenAdmin();
            } else if ("chef".equals(login.nameField.getText()) && "chef".equals(login.passwordField.getText())) {
                new WelcomeScreenChef();
                login.jf.dispose();
            } else {
                login.invalid.setVisible(true);
            }
        }
    }

}