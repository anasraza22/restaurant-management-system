
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class MenuItemsPannel {

    JFrame jf;
    JLabel[] lable = new JLabel[78];
    JCheckBox[] box = new JCheckBox[67];
    JPanel jp;
    JButton jb;
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    JTextField[] textField = new JTextField[67];
    MenuTextFieldHandler hand = new MenuTextFieldHandler();
    MenuCheckBoxHandler boxHandler = new MenuCheckBoxHandler(this);

    public MenuItemsPannel() {
        init_GUI();

    }

    private void init_GUI() {
        jf = new JFrame("Menu");
        jf.setLayout(null);

        lable[67] = new JLabel("MENU");
        lable[67].setBounds(d.width / 2 - 60, 0, 200, 50);
        lable[67].setFocusable(false);
        lable[67].setFont(new Font("verdana", Font.BOLD, 32));
        jf.add(lable[67]);

        //----------------------1st Coloum menu---------------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(10, 50, 450, 180);
        Border b = BorderFactory.createLineBorder(Color.GRAY, 2, true);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(false);
        jf.setLocation(-7, -1);
        jf.setSize(d.width + 15, d.height + 3);
        jf.setVisible(true);

        //----------------------1st Coloum, 1st Row menu------------------------
        lable[68] = new JLabel("SOUPS");
        lable[68].setBounds(180, 2, 80, 20);
        lable[68].setFocusable(false);
        lable[68].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[68]);

        lable[69] = new JLabel("VEG");
        lable[69].setBounds(5, 24, 40, 10);
        lable[69].setFocusable(false);
        lable[69].setFont(new Font("verdana", Font.BOLD, 12));
        jp.add(lable[69]);

        box[0] = new JCheckBox("Tomato Soup", false);
        box[0].setBounds(5, 40, 100, 20);
        lable[0] = new JLabel("100.0/-");
        lable[0].setBounds(300, 40, 100, 20);
        textField[0] = new JTextField();
        textField[0].setBounds(365, 40, 70, 20);
        jp.add(box[0]);
        jp.add(lable[0]);
        jp.add(textField[0]);

        box[1] = new JCheckBox("Hot & Sour", false);
        box[1].setBounds(5, 64, 100, 20);
        lable[1] = new JLabel("100.0/-");
        lable[1].setBounds(300, 64, 100, 20);
        textField[1] = new JTextField();
        textField[1].setBounds(365, 64, 70, 20);
        jp.add(box[1]);
        jp.add(lable[1]);
        jp.add(textField[1]);

        box[2] = new JCheckBox("Sweet Corn Soup", false);
        box[2].setBounds(5, 88, 130, 20);
        lable[2] = new JLabel("100.0/-");
        lable[2].setBounds(300, 88, 100, 20);
        textField[2] = new JTextField();
        textField[2].setBounds(365, 88, 70, 20);
        jp.add(box[2]);
        jp.add(lable[2]);
        jp.add(textField[2]);

        lable[70] = new JLabel("NON-VEG");
        lable[70].setBounds(5, 114, 80, 10);
        lable[70].setFocusable(false);
        lable[70].setFont(new Font("verdana", Font.BOLD, 12));
        jp.add(lable[70]);

        box[3] = new JCheckBox("Chicken Corn Soup", false);
        box[3].setBounds(5, 130, 150, 20);
        lable[3] = new JLabel("150.0/-");
        lable[3].setBounds(300, 130, 100, 20);
        textField[3] = new JTextField();
        textField[3].setBounds(365, 130, 70, 20);
        jp.add(box[3]);
        jp.add(lable[3]);
        jp.add(textField[3]);

        box[4] = new JCheckBox("Chicken Hot & Sour", false);
        box[4].setBounds(5, 154, 150, 20);
        lable[4] = new JLabel("150.0/-");
        lable[4].setBounds(300, 154, 100, 20);
        textField[4] = new JTextField();
        textField[4].setBounds(365, 154, 70, 20);
        jp.add(box[4]);
        jp.add(lable[4]);
        jp.add(textField[4]);

        //----------------------1st Coloum, 2nd Row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(10, 235, 450, 243);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[71] = new JLabel("CHICKEN HANDI");
        lable[71].setBounds(130, 2, 180, 20);
        lable[71].setFocusable(false);
        lable[71].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[71]);

        box[5] = new JCheckBox("Chicken Karahi(Half)", false);
        box[5].setBounds(5, 25, 150, 20);
        lable[5] = new JLabel("450.0/-");
        lable[5].setBounds(300, 25, 100, 20);
        textField[5] = new JTextField();
        textField[5].setBounds(365, 25, 70, 20);
        jp.add(box[5]);
        jp.add(lable[5]);
        jp.add(textField[5]);

        box[6] = new JCheckBox("Chicken Karahi(Full)", false);
        box[6].setBounds(5, 49, 150, 20);
        lable[6] = new JLabel("750.0/-");
        lable[6].setBounds(300, 49, 100, 20);
        textField[6] = new JTextField();
        textField[6].setBounds(365, 49, 70, 20);
        jp.add(box[6]);
        jp.add(lable[6]);
        jp.add(textField[6]);

        box[7] = new JCheckBox("Chicken Boneless", false);
        box[7].setBounds(5, 73, 150, 20);
        lable[7] = new JLabel("800.0/-");
        lable[7].setBounds(300, 73, 100, 20);
        textField[7] = new JTextField();
        textField[7].setBounds(365, 73, 70, 20);
        jp.add(box[7]);
        jp.add(lable[7]);
        jp.add(textField[7]);

        box[8] = new JCheckBox("Chicken Tikka Masala", false);
        box[8].setBounds(5, 97, 150, 20);
        lable[8] = new JLabel("750.0/-");
        lable[8].setBounds(300, 97, 100, 20);
        textField[8] = new JTextField();
        textField[8].setBounds(365, 97, 70, 20);
        jp.add(box[8]);
        jp.add(lable[8]);
        jp.add(textField[8]);

        box[9] = new JCheckBox("Chicken Qorma", false);
        box[9].setBounds(5, 121, 150, 20);
        lable[9] = new JLabel("750.0/-");
        lable[9].setBounds(300, 121, 100, 20);
        textField[9] = new JTextField();
        textField[9].setBounds(365, 121, 70, 20);
        jp.add(box[9]);
        jp.add(lable[9]);
        jp.add(textField[9]);

        box[10] = new JCheckBox("Chicken Jalfarazi", false);
        box[10].setBounds(5, 145, 150, 20);
        lable[10] = new JLabel("850.0/-");
        lable[10].setBounds(300, 145, 100, 20);
        textField[10] = new JTextField();
        textField[10].setBounds(365, 145, 70, 20);
        jp.add(box[10]);
        jp.add(lable[10]);
        jp.add(textField[10]);

        box[11] = new JCheckBox("Chicken Makhni Handi", false);
        box[11].setBounds(5, 169, 150, 20);
        lable[11] = new JLabel("800.0/-");
        lable[11].setBounds(300, 169, 100, 20);
        textField[11] = new JTextField();
        textField[11].setBounds(365, 169, 70, 20);
        jp.add(box[11]);
        jp.add(lable[11]);
        jp.add(textField[11]);

        box[12] = new JCheckBox("Chicken Ichari Handi", false);
        box[12].setBounds(5, 193, 150, 20);
        lable[12] = new JLabel("800.0/-");
        lable[12].setBounds(300, 193, 100, 20);
        textField[12] = new JTextField();
        textField[12].setBounds(365, 193, 70, 20);
        jp.add(box[12]);
        jp.add(lable[12]);
        jp.add(textField[12]);

        box[13] = new JCheckBox("White Chicken Handi", false);
        box[13].setBounds(5, 217, 150, 20);
        lable[13] = new JLabel("800.0/-");
        lable[13].setBounds(300, 217, 100, 20);
        textField[13] = new JTextField();
        textField[13].setBounds(365, 217, 70, 20);
        jp.add(box[13]);
        jp.add(lable[13]);
        jp.add(textField[13]);

        //----------------------1st Coloum, 3rd Row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(10, 483, 450, 202);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[72] = new JLabel("MUTTON HANDI");
        lable[72].setBounds(130, 2, 180, 20);
        lable[72].setFocusable(false);
        lable[72].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[72]);

        box[14] = new JCheckBox("Mutton Karahi(Half)", false);
        box[14].setBounds(5, 25, 150, 20);
        lable[14] = new JLabel("1000/-");
        lable[14].setBounds(300, 25, 100, 20);
        textField[14] = new JTextField();
        textField[14].setBounds(365, 25, 70, 20);
        jp.add(box[14]);
        jp.add(lable[14]);
        jp.add(textField[14]);

        box[15] = new JCheckBox("Mutton Karahi(Full)", false);
        box[15].setBounds(5, 49, 150, 20);
        lable[15] = new JLabel("1500/-");
        lable[15].setBounds(300, 49, 100, 20);
        textField[15] = new JTextField();
        textField[15].setBounds(365, 49, 70, 20);
        jp.add(box[15]);
        jp.add(lable[15]);
        jp.add(textField[15]);

        box[16] = new JCheckBox("Mutton Kunna", false);
        box[16].setBounds(5, 73, 150, 20);
        lable[16] = new JLabel("1650/-");
        lable[16].setBounds(300, 73, 100, 20);
        textField[16] = new JTextField();
        textField[16].setBounds(365, 73, 70, 20);
        jp.add(box[16]);
        jp.add(lable[16]);
        jp.add(textField[16]);

        box[17] = new JCheckBox("Mutton Ginger", false);
        box[17].setBounds(5, 97, 150, 20);
        lable[17] = new JLabel("1500/-");
        lable[17].setBounds(300, 97, 100, 20);
        textField[17] = new JTextField();
        textField[17].setBounds(365, 97, 70, 20);
        jp.add(box[17]);
        jp.add(lable[17]);
        jp.add(textField[17]);

        box[18] = new JCheckBox("Mutton Qorma", false);
        box[18].setBounds(5, 121, 150, 20);
        lable[18] = new JLabel("1650/-");
        lable[18].setBounds(300, 121, 100, 20);
        textField[18] = new JTextField();
        textField[18].setBounds(365, 121, 70, 20);
        jp.add(box[18]);
        jp.add(lable[18]);
        jp.add(textField[18]);

        box[19] = new JCheckBox("Mutton Green Masala", false);
        box[19].setBounds(5, 145, 150, 20);
        lable[19] = new JLabel("1650/-");
        lable[19].setBounds(300, 145, 100, 20);
        textField[19] = new JTextField();
        textField[19].setBounds(365, 145, 70, 20);
        jp.add(box[19]);
        jp.add(lable[19]);
        jp.add(textField[19]);

        box[20] = new JCheckBox("Mutton Ichari Handi", false);
        box[20].setBounds(5, 169, 150, 20);
        lable[20] = new JLabel("1650/-");
        lable[20].setBounds(300, 169, 100, 20);
        textField[20] = new JTextField();
        textField[20].setBounds(365, 169, 70, 20);
        jp.add(box[20]);
        jp.add(lable[20]);
        jp.add(textField[20]);

        //----------------------2nd Coloum, 1st row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(462, 50, 450, 219);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[73] = new JLabel("FISH");
        lable[73].setBounds(180, 2, 80, 20);
        lable[73].setFocusable(false);
        lable[73].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[73]);

        box[21] = new JCheckBox("Crispy Fried Fish", false);
        box[21].setBounds(5, 25, 150, 20);
        lable[21] = new JLabel("300.0/-");
        lable[21].setBounds(300, 25, 100, 20);
        textField[21] = new JTextField();
        textField[21].setBounds(365, 25, 70, 20);
        jp.add(box[21]);
        jp.add(lable[21]);
        jp.add(textField[21]);

        box[22] = new JCheckBox("Fish Manchurian", false);
        box[22].setBounds(5, 49, 150, 20);;
        lable[22] = new JLabel("700.0/-");
        lable[22].setBounds(300, 49, 100, 20);
        textField[22] = new JTextField();
        textField[22].setBounds(365, 49, 70, 20);
        jp.add(box[22]);
        jp.add(lable[22]);
        jp.add(textField[22]);

        box[23] = new JCheckBox("Spicy Tawa Fish", false);
        box[23].setBounds(5, 73, 150, 20);
        lable[23] = new JLabel("450.0/-");
        lable[23].setBounds(300, 73, 100, 20);
        textField[23] = new JTextField();
        textField[23].setBounds(365, 73, 70, 20);
        jp.add(box[23]);
        jp.add(lable[23]);
        jp.add(textField[23]);

        box[24] = new JCheckBox("Grilled Pawns", false);
        box[24].setBounds(5, 97, 150, 20);
        lable[24] = new JLabel("750.0/-");
        lable[24].setBounds(300, 97, 100, 20);
        textField[24] = new JTextField();
        textField[24].setBounds(365, 97, 70, 20);
        jp.add(box[24]);
        jp.add(lable[24]);
        jp.add(textField[24]);

        box[25] = new JCheckBox("Fish Salan", false);
        box[25].setBounds(5, 121, 150, 20);
        lable[25] = new JLabel("500.0/-");
        lable[25].setBounds(300, 121, 100, 20);
        textField[25] = new JTextField();
        textField[25].setBounds(365, 121, 70, 20);
        jp.add(box[25]);
        jp.add(lable[25]);
        jp.add(textField[25]);

        box[26] = new JCheckBox("Dhaka Fried Fish", false);
        box[26].setBounds(5, 145, 150, 20);
        lable[26] = new JLabel("650.0/-");
        lable[26].setBounds(300, 145, 100, 20);
        textField[26] = new JTextField();
        textField[26].setBounds(365, 145, 70, 20);
        jp.add(box[26]);
        jp.add(lable[26]);
        jp.add(textField[26]);

        box[27] = new JCheckBox("Finger Fish", false);
        box[27].setBounds(5, 169, 150, 20);
        lable[27] = new JLabel("450.0/-");
        lable[27].setBounds(300, 169, 100, 20);
        textField[27] = new JTextField();
        textField[27].setBounds(365, 169, 70, 20);
        jp.add(box[27]);
        jp.add(lable[27]);
        jp.add(textField[27]);

        box[28] = new JCheckBox("Pomfret Fish Fry", false);
        box[28].setBounds(5, 193, 150, 20);
        lable[28] = new JLabel("300.0/-");
        lable[28].setBounds(300, 193, 100, 20);
        textField[28] = new JTextField();
        textField[28].setBounds(365, 193, 70, 20);
        jp.add(box[28]);
        jp.add(lable[28]);
        jp.add(textField[28]);

        //----------------------2nd Coloum, 2nd row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(462, 275, 450, 410);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[74] = new JLabel("B.B.Q");
        lable[74].setBounds(180, 2, 180, 20);
        lable[74].setFocusable(false);
        lable[74].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[74]);

        box[29] = new JCheckBox("Chicken Tikka", false);
        box[29].setBounds(5, 25, 150, 20);
        lable[29] = new JLabel("400.0/-");
        lable[29].setBounds(300, 25, 100, 20);
        textField[29] = new JTextField();
        textField[29].setBounds(365, 25, 70, 20);
        jp.add(box[29]);
        jp.add(lable[29]);
        jp.add(textField[29]);

        box[30] = new JCheckBox("Chicken Malai Boti", false);
        box[30].setBounds(5, 49, 150, 20);
        lable[30] = new JLabel("550.0/-");
        lable[30].setBounds(300, 49, 100, 20);
        textField[30] = new JTextField();
        textField[30].setBounds(365, 49, 70, 20);
        jp.add(box[30]);
        jp.add(lable[30]);
        jp.add(textField[30]);

        box[31] = new JCheckBox("Chicken Kabab", false);
        box[31].setBounds(5, 73, 150, 20);
        lable[31] = new JLabel("450.0/-");
        lable[31].setBounds(300, 73, 100, 20);
        textField[31] = new JTextField();
        textField[31].setBounds(365, 73, 70, 20);
        jp.add(box[31]);
        jp.add(lable[31]);
        jp.add(textField[31]);

        box[32] = new JCheckBox("Behari Kabab", false);
        box[32].setBounds(5, 97, 150, 20);
        lable[32] = new JLabel("450.0/-");
        lable[32].setBounds(300, 97, 100, 20);
        textField[32] = new JTextField();
        textField[32].setBounds(365, 97, 70, 20);
        jp.add(box[32]);
        jp.add(lable[32]);
        jp.add(textField[32]);

        box[33] = new JCheckBox("Reshmi Kabab", false);
        box[33].setBounds(5, 121, 150, 20);
        lable[33] = new JLabel("400.0/-");
        lable[33].setBounds(300, 121, 100, 20);
        textField[33] = new JTextField();
        textField[33].setBounds(365, 121, 70, 20);
        jp.add(box[33]);
        jp.add(lable[33]);
        jp.add(textField[33]);

        box[34] = new JCheckBox("Lahori Chargha", false);
        box[34].setBounds(5, 145, 150, 20);
        lable[34] = new JLabel("800.0/-");
        lable[34].setBounds(300, 145, 100, 20);
        textField[34] = new JTextField();
        textField[34].setBounds(365, 145, 70, 20);
        jp.add(box[34]);
        jp.add(lable[34]);
        jp.add(textField[34]);

        box[35] = new JCheckBox("Grilled Pomfret", false);
        box[35].setBounds(5, 169, 150, 20);
        lable[35] = new JLabel("650.0/-");
        lable[35].setBounds(300, 169, 100, 20);
        textField[35] = new JTextField();
        textField[35].setBounds(365, 169, 70, 20);
        jp.add(box[35]);
        jp.add(lable[35]);
        jp.add(textField[35]);

        box[36] = new JCheckBox("Chicken Liver", false);
        box[36].setBounds(5, 193, 150, 20);
        lable[36] = new JLabel("500.0/-");
        lable[36].setBounds(300, 193, 100, 20);
        textField[36] = new JTextField();
        textField[36].setBounds(365, 193, 70, 20);
        jp.add(box[36]);
        jp.add(lable[36]);
        jp.add(textField[36]);

        box[37] = new JCheckBox("Gola Kabab", false);
        box[37].setBounds(5, 217, 150, 20);
        lable[37] = new JLabel("650.0/-");
        lable[37].setBounds(300, 217, 100, 20);
        textField[37] = new JTextField();
        textField[37].setBounds(365, 217, 70, 20);
        jp.add(box[37]);
        jp.add(lable[37]);
        jp.add(textField[37]);

        box[38] = new JCheckBox("Mutton Ribs", false);
        box[38].setBounds(5, 241, 150, 20);
        lable[38] = new JLabel("950.0/-");
        lable[38].setBounds(300, 241, 100, 20);
        textField[38] = new JTextField();
        textField[38].setBounds(365, 241, 70, 20);
        jp.add(box[38]);
        jp.add(lable[38]);
        jp.add(textField[38]);

        box[39] = new JCheckBox("Afghani Kabab", false);
        box[39].setBounds(5, 265, 150, 20);
        lable[39] = new JLabel("450.0/-");
        lable[39].setBounds(300, 265, 100, 20);
        textField[39] = new JTextField();
        textField[39].setBounds(365, 265, 70, 20);
        jp.add(box[39]);
        jp.add(lable[39]);
        jp.add(textField[39]);

        box[40] = new JCheckBox("Fish Tikka", false);
        box[40].setBounds(5, 289, 150, 20);
        lable[40] = new JLabel("650.0/-");
        lable[40].setBounds(300, 289, 100, 20);
        textField[40] = new JTextField();
        textField[40].setBounds(365, 289, 70, 20);
        jp.add(box[40]);
        jp.add(lable[40]);
        jp.add(textField[40]);

        box[41] = new JCheckBox("Tawa Piece", false);
        box[41].setBounds(5, 313, 150, 20);
        lable[41] = new JLabel("200.0/-");
        lable[41].setBounds(300, 313, 100, 20);
        textField[41] = new JTextField();
        textField[41].setBounds(365, 313, 70, 20);
        jp.add(box[41]);
        jp.add(lable[41]);
        jp.add(textField[41]);

        box[42] = new JCheckBox("Chappal Kabab", false);
        box[42].setBounds(5, 337, 150, 20);
        lable[42] = new JLabel("300.0/-");
        lable[42].setBounds(300, 337, 100, 20);
        textField[42] = new JTextField();
        textField[42].setBounds(365, 337, 70, 20);
        jp.add(box[42]);
        jp.add(lable[42]);
        jp.add(textField[42]);

        box[43] = new JCheckBox("Chicken Malai Tikka", false);
        box[43].setBounds(5, 361, 150, 20);
        lable[43] = new JLabel("450.0/-");
        lable[43].setBounds(300, 361, 100, 20);
        textField[43] = new JTextField();
        textField[43].setBounds(365, 361, 70, 20);
        jp.add(box[43]);
        jp.add(lable[43]);
        jp.add(textField[43]);

        box[44] = new JCheckBox("Paneer Tikka", false);
        box[44].setBounds(5, 385, 150, 20);
        lable[44] = new JLabel("450.0/-");
        lable[44].setBounds(300, 385, 100, 20);
        textField[44] = new JTextField();
        textField[44].setBounds(365, 385, 70, 20);
        jp.add(box[44]);
        jp.add(lable[44]);
        jp.add(textField[44]);

        //----------------------3rd Coloum, 1st row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(914, 50, 450, 195);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[75] = new JLabel("RICE");
        lable[75].setBounds(190, 2, 80, 20);
        lable[75].setFocusable(false);
        lable[75].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[75]);

        box[45] = new JCheckBox("Chicken Pulao", false);
        box[45].setBounds(5, 25, 150, 20);
        lable[45] = new JLabel("120.0/-");
        lable[45].setBounds(300, 25, 100, 20);
        textField[45] = new JTextField();
        textField[45].setBounds(365, 25, 70, 20);
        jp.add(box[45]);
        jp.add(lable[45]);
        jp.add(textField[45]);

        box[46] = new JCheckBox("Egg Fried Rice", false);
        box[46].setBounds(5, 49, 150, 20);
        lable[46] = new JLabel("150.0/-");
        lable[46].setBounds(300, 49, 100, 20);
        textField[46] = new JTextField();
        textField[46].setBounds(365, 49, 70, 20);
        jp.add(box[46]);
        jp.add(lable[46]);
        jp.add(textField[46]);

        box[47] = new JCheckBox("Chicken Biryani (Half)", false);
        box[47].setBounds(5, 73, 150, 20);
        lable[47] = new JLabel("200.0/-");
        lable[47].setBounds(300, 73, 100, 20);
        textField[47] = new JTextField();
        textField[47].setBounds(365, 73, 70, 20);
        jp.add(box[47]);
        jp.add(lable[47]);
        jp.add(textField[47]);

        box[48] = new JCheckBox("Chicken Biryani (Full)", false);
        box[48].setBounds(5, 97, 150, 20);
        lable[48] = new JLabel("300.0/-");
        lable[48].setBounds(300, 97, 100, 20);
        textField[48] = new JTextField();
        textField[48].setBounds(365, 97, 70, 20);
        jp.add(box[48]);
        jp.add(lable[48]);
        jp.add(textField[48]);

        box[49] = new JCheckBox("Sindhi Dum Biryani", false);
        box[49].setBounds(5, 121, 150, 20);
        lable[49] = new JLabel("250.0/-");
        lable[49].setBounds(300, 121, 100, 20);
        textField[49] = new JTextField();
        textField[49].setBounds(365, 121, 70, 20);
        jp.add(box[49]);
        jp.add(lable[49]);
        jp.add(textField[49]);

        box[50] = new JCheckBox("Zeera Rice", false);
        box[50].setBounds(5, 145, 150, 20);
        lable[50] = new JLabel("150.0/-");
        lable[50].setBounds(300, 145, 100, 20);
        textField[50] = new JTextField();
        textField[50].setBounds(365, 145, 70, 20);
        jp.add(box[50]);
        jp.add(lable[50]);
        jp.add(textField[50]);

        box[51] = new JCheckBox("Zarda", false);
        box[51].setBounds(5, 169, 150, 20);
        lable[51] = new JLabel("200.0/-");
        lable[51].setBounds(300, 169, 100, 20);
        textField[51] = new JTextField();
        textField[51].setBounds(365, 169, 70, 20);
        jp.add(box[51]);
        jp.add(lable[51]);
        jp.add(textField[51]);

        //----------------------3rd Coloum, 2nd row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(914, 250, 450, 195);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[76] = new JLabel("TUNDOOR");
        lable[76].setBounds(170, 2, 150, 20);
        lable[76].setFocusable(false);
        lable[76].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[76]);

        box[52] = new JCheckBox("Roghni Naan", false);
        box[52].setBounds(5, 25, 150, 20);
        lable[52] = new JLabel("55.00/-");
        lable[52].setBounds(300, 25, 100, 20);
        textField[52] = new JTextField();
        textField[52].setBounds(365, 25, 70, 20);
        jp.add(box[52]);
        jp.add(lable[52]);
        jp.add(textField[52]);

        box[53] = new JCheckBox("Whole Wheet Naan", false);
        box[53].setBounds(5, 49, 150, 20);
        lable[53] = new JLabel("40.00/-");
        lable[53].setBounds(300, 49, 100, 20);
        textField[53] = new JTextField();
        textField[53].setBounds(365, 49, 70, 20);
        jp.add(box[53]);
        jp.add(lable[53]);
        jp.add(textField[53]);

        box[54] = new JCheckBox("Garlic Naan", false);
        box[54].setBounds(5, 73, 150, 20);
        lable[54] = new JLabel("45.00/-");
        lable[54].setBounds(300, 73, 100, 20);
        textField[54] = new JTextField();
        textField[54].setBounds(365, 73, 70, 20);
        jp.add(box[54]);
        jp.add(lable[54]);
        jp.add(textField[54]);

        box[55] = new JCheckBox("Paratha", false);
        box[55].setBounds(5, 97, 150, 20);
        lable[55] = new JLabel("45.00/-");
        lable[55].setBounds(300, 97, 100, 20);
        textField[55] = new JTextField();
        textField[55].setBounds(365, 97, 70, 20);
        jp.add(box[55]);
        jp.add(lable[55]);
        jp.add(textField[55]);

        box[56] = new JCheckBox("Chicken Qeema Naan", false);
        box[56].setBounds(5, 121, 150, 20);
        lable[56] = new JLabel("450.0/-");
        lable[56].setBounds(300, 121, 100, 20);
        textField[56] = new JTextField();
        textField[56].setBounds(365, 121, 70, 20);
        jp.add(box[56]);
        jp.add(lable[56]);
        jp.add(textField[56]);

        box[57] = new JCheckBox("Plain Naan", false);
        box[57].setBounds(5, 145, 150, 20);
        lable[57] = new JLabel("30.00/-");
        lable[57].setBounds(300, 145, 100, 20);
        textField[57] = new JTextField();
        textField[57].setBounds(365, 145, 70, 20);
        jp.add(box[57]);
        jp.add(lable[57]);
        jp.add(textField[57]);

        box[58] = new JCheckBox("Roti", false);
        box[58].setBounds(5, 169, 150, 20);
        lable[58] = new JLabel("15.00/-");
        lable[58].setBounds(300, 169, 100, 20);
        textField[58] = new JTextField();
        textField[58].setBounds(365, 169, 70, 20);
        jp.add(box[58]);
        jp.add(lable[58]);
        jp.add(textField[58]);

        //----------------------3rd Coloum, 3rd row menu------------------------
        jp = new JPanel();
        jp.setLayout(null);
        jp.setBounds(914, 450, 450, 235);
        jp.setBorder(b);
        jp.setBackground(Color.WHITE);
        jf.add(jp);

        lable[77] = new JLabel("Beverages");
        lable[77].setBounds(170, 2, 150, 20);
        lable[77].setFocusable(false);
        lable[77].setFont(new Font("verdana", Font.BOLD, 18));
        jp.add(lable[77]);

        box[59] = new JCheckBox("Cold Drink", false);
        box[59].setBounds(5, 25, 150, 20);
        lable[59] = new JLabel("45.00/-");
        lable[59].setBounds(300, 25, 100, 20);
        textField[59] = new JTextField();
        textField[59].setBounds(365, 25, 70, 20);
        jp.add(box[59]);
        jp.add(lable[59]);
        jp.add(textField[59]);

        box[60] = new JCheckBox("Fresh Lime", false);
        box[60].setBounds(5, 49, 150, 20);
        lable[60] = new JLabel("70.00/-");
        lable[60].setBounds(300, 49, 100, 20);
        textField[60] = new JTextField();
        textField[60].setBounds(365, 49, 70, 20);
        jp.add(box[60]);
        jp.add(lable[60]);
        jp.add(textField[60]);

        box[61] = new JCheckBox("Lassi", false);
        box[61].setBounds(5, 73, 150, 20);
        lable[61] = new JLabel("135.0/-");
        lable[61].setBounds(300, 73, 100, 20);
        textField[61] = new JTextField();
        textField[61].setBounds(365, 73, 70, 20);
        jp.add(box[61]);
        jp.add(lable[61]);
        jp.add(textField[61]);

        box[62] = new JCheckBox("Mineral Water (0.5L)", false);
        box[62].setBounds(5, 97, 150, 20);
        lable[62] = new JLabel("50.00/-");
        lable[62].setBounds(300, 97, 100, 20);
        textField[62] = new JTextField();
        textField[62].setBounds(365, 97, 70, 20);
        jp.add(box[62]);
        jp.add(lable[62]);
        jp.add(textField[62]);

        box[63] = new JCheckBox("Mineral Water (1L)", false);
        box[63].setBounds(5, 121, 150, 20);
        lable[63] = new JLabel("100.0/-");
        lable[63].setBounds(300, 121, 100, 20);
        textField[63] = new JTextField();
        textField[63].setBounds(365, 121, 70, 20);
        jp.add(box[63]);
        jp.add(lable[63]);
        jp.add(textField[63]);

        box[64] = new JCheckBox("Chai", false);
        box[64].setBounds(5, 145, 150, 20);
        lable[64] = new JLabel("50.00/-");
        lable[64].setBounds(300, 145, 100, 20);
        textField[64] = new JTextField();
        textField[64].setBounds(365, 145, 70, 20);
        jp.add(box[64]);
        jp.add(lable[64]);
        jp.add(textField[64]);

        box[65] = new JCheckBox("Green Tea", false);
        box[65].setBounds(5, 169, 150, 20);
        lable[65] = new JLabel("50.00/-");
        lable[65].setBounds(300, 169, 100, 20);
        textField[65] = new JTextField();
        textField[65].setBounds(365, 169, 70, 20);
        jp.add(box[65]);
        jp.add(lable[65]);
        jp.add(textField[65]);

        box[66] = new JCheckBox("Coffee", false);
        box[66].setBounds(5, 193, 150, 20);
        lable[66] = new JLabel("250.00/-");
        lable[66].setBounds(300, 193, 100, 20);
        textField[66] = new JTextField();
        textField[66].setBounds(365, 193, 70, 20);
        jp.add(box[66]);
        jp.add(lable[66]);
        jp.add(textField[66]);

        jb = new JButton("Done");
        jb.setBounds(10, 690, 1353, 50);
        jb.setFont(new Font("verdana", Font.BOLD, 50));
        jb.addActionListener(boxHandler);
        jb.setFocusable(false);
        jf.add(jb);

        for (int i = 0; i <= 66; i++) {
            box[i].setFocusable(false);
            box[i].addActionListener(boxHandler);

            lable[i].setFocusable(false);
            lable[i].setFont(new Font("verdana", Font.BOLD, 12));

            textField[i].setText("1");
            textField[i].setEnabled(false);
            textField[i].addKeyListener(hand);
        }

    }

    JFrame getFrame() {
        return jf;
    }

}
