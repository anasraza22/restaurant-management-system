
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class WelcomeScreenChef {

    JFrame jf;
    JButton order;
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    BtnHandlerChef btnHandlr;

    public WelcomeScreenChef() {
        init_GUI();
    }

    private void init_GUI() {
        jf = new JFrame("RESTAURENT");
        jf.setLayout(null);

        order = new JButton("CHECK ORDER LIST");
        order.setBounds(d.width / 2 - 150, d.height / 2 - 100, 300, 100);
        order.setFont(new Font("verdana", Font.BOLD, 20));
        order.setFocusable(false);
        btnHandlr = new BtnHandlerChef(this);
        order.addActionListener(btnHandlr);
        jf.add(order);

        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(false);
        jf.setLocation(-7, -1);
        jf.setSize(d.width + 15, d.height + 3);
        jf.setVisible(true);

    }

}
