
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class Invoice {

    JFrame jf;
    JLabel jl;
    JButton confrm, edit;
    int orderId;
    DBO dbo = new DBO();
    JTable jt;
    MenuItemsPannel menuItemsPannel;
    InvoiceBtnHandler btnHandler = new InvoiceBtnHandler(this);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

    public Invoice(int orderId, MenuItemsPannel menu) throws SQLException {
        this.orderId = orderId;
        menuItemsPannel = menu;
        initGUI();
    }

    public Invoice(int orderId) throws SQLException {
        this.orderId = orderId;
        initGUI();
    }

    private void initGUI() throws SQLException {
        jf = new JFrame("INVOICE");
        jf.setLayout(null);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(false);
        jf.setLocation(-7, -1);
        jf.setSize(d.width + 15, d.height + 3);
        jf.setVisible(true);

        jl = new JLabel("YOUR DESIRED ORDER");
        jl.setBounds(d.width / 2 - 200, 0, 500, 50);
        jl.setFocusable(false);
        jl.setFont(new Font("verdana", Font.BOLD, 32));
        jf.add(jl);

        jl = new JLabel();
        jl.setBounds(200, 100, 500, 50);
        jl.setFocusable(false);
        jl.setFont(new Font("verdana", Font.BOLD, 15));
        jl.setText("Your Order no is: " + orderId + "");
        jf.add(jl);

        confrm = new JButton("Confirm");
        confrm.setBounds(1250, 650, 90, 20);
        confrm.setFocusable(false);
        confrm.addActionListener(btnHandler);
        jf.add(confrm);

        edit = new JButton("Go Back");
        edit.setBounds(1150, 650, 90, 20);
        edit.setFocusable(false);
        edit.addActionListener(btnHandler);
        jf.add(edit);

        Vector<String> columnNames = new Vector();
        columnNames.add("Sr #");
        columnNames.add("Name");
        columnNames.add("Quantity");
        columnNames.add("Unit Price");
        columnNames.add("Total");
        Vector<Vector<String>> rowData = new Vector<Vector<String>>();

        ResultSet rs = dbo.getOrderDetails(orderId);
        while (rs.next()) {
            Vector<String> record = new Vector<String>();
            record.add(rs.getString("Item No"));
            record.add(rs.getString("Item Name"));
            record.add(rs.getString("Quantity"));
            record.add(rs.getString("Price"));
            record.add((Double.parseDouble(rs.getString("Quantity")) * Double.parseDouble(rs.getString("Price").substring(0, rs.getString("Price").length() - 2))) + "/-");
            rowData.add(record);
        }

        jt = new JTable(rowData, columnNames);
        JScrollPane jsp = new JScrollPane(jt);
        jsp.setBounds(400, 200, 600, 200);
        jf.add(jsp);
    }

    void diposeMenu() {
        menuItemsPannel.getFrame().dispose();
    }

}
