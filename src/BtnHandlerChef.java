

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class BtnHandlerChef implements ActionListener {

    WelcomeScreenChef wc;

    public BtnHandlerChef(WelcomeScreenChef wc) {
        this.wc = wc;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == wc.order) {
            new ChefPannel();
            wc.jf.dispose();
        }
    }

}
