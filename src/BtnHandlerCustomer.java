

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class BtnHandlerCustomer implements ActionListener {

    WelcomeScreenCustomer ws;

    public BtnHandlerCustomer(WelcomeScreenCustomer ws) {
        this.ws = ws;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == ws.order) {
            new MenuItemsPannel();
            ws.jf.dispose();
        }
    }

}
