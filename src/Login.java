

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class Login {

    JFrame jf;
    JLabel head, name, pass, invalid;
    JTextField nameField;
    JPasswordField passwordField;
    JButton loginButton;
    LoginBtnHandler btn;
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

    public Login() {
        init_GUI();
    }

    private void init_GUI() {
        jf = new JFrame("Login");
        jf.setLayout(null);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(false);
        jf.setLocation(-7, -1);
        jf.setSize(d.width + 15, d.height + 3);
        jf.setVisible(true);

        head = new JLabel("LOGIN");
        head.setBounds(d.width / 2 - 80, 100, 200, 50);
        head.setFocusable(false);
        head.setFont(new Font("verdana", Font.BOLD, 32));
        jf.add(head);

        name = new JLabel("NAME: ");
        name.setBounds(d.width / 3, 200, 100, 50);
        name.setFocusable(false);
        name.setFont(new Font("verdana", Font.BOLD, 14));
        jf.add(name);

        pass = new JLabel("PASSWORD: ");
        pass.setBounds(d.width / 3, 240, 120, 50);
        pass.setFocusable(false);
        pass.setFont(new Font("verdana", Font.BOLD, 14));
        jf.add(pass);

        nameField = new JTextField();
        nameField.setBounds(d.width / 3 + 150, 215, 200, 20);
        nameField.setFont(new Font("verdana", Font.PLAIN, 12));
        jf.add(nameField);

        passwordField = new JPasswordField();
        passwordField.setBounds(d.width / 3 + 150, 255, 200, 20);
        jf.add(passwordField);

        loginButton = new JButton("Login");
        loginButton.setBounds(d.width / 3 + 150, 295, 80, 30);
        loginButton.setFont(new Font("verdana", Font.BOLD, 14));
        btn = new LoginBtnHandler(this);
        loginButton.addActionListener(btn);
        jf.add(loginButton);

        invalid = new JLabel("Your Username or Password is Invalid");
        invalid.setBounds(d.width / 3 + 50, 400, 500, 50);
        invalid.setFocusable(false);
        invalid.setForeground(Color.red);
        invalid.setFont(new Font("verdana", Font.BOLD, 14));
        invalid.setVisible(false);
        jf.add(invalid);
    }

}
