

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class BtnHandlerAdmin implements ActionListener {

    WelcomeScreenAdmin wa;

    public BtnHandlerAdmin(WelcomeScreenAdmin wa) {
        this.wa = wa;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
         if (e.getSource() == wa.order) {
            new AdminPannel();
            wa.jf.dispose();
        }
    }

}
