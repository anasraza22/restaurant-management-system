import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.rmi.Naming;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class Reciept {

    JFrame jf;
    JLabel jl;
    JTextArea area;
    JButton jb;
    JPanel jp;
    int orderID;
    RestaurentServer server;
    DBO dbo = new DBO();
    RecieptBtnHandler btnHandler;
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();

    public Reciept(int orderID) throws Exception {
        this.orderID = orderID;
        init_GUI();
        connect();
        server.sendOrderToCashier(orderID);
    }

    private void init_GUI() throws SQLException {
        jf = new JFrame("Your Invoice");
        jf.setLayout(null);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(false);
        jf.setLocation(-7, -1);
        jf.setSize(d.width + 15, d.height + 3);
        jf.setVisible(true);

        jl = new JLabel("Please pay your bill to proceed your order, Thank you!");
        jl.setBounds(200, 0, 1000, 50);
        jl.setFocusable(false);
        jl.setFont(new Font("verdana", Font.BOLD, 32));
        jf.add(jl);

        jp = new JPanel(null);
        jp.setBackground(Color.red);
        jp.setBounds(400, 100, 540, 600);
        jf.add(jp);

        jb = new JButton("Finish");
        jb.setBounds(jp.getWidth() + jp.getX() + 100, jp.getY() + jp.getHeight() - 30, 100, 30);
        jb.setFocusable(false);

        btnHandler = new RecieptBtnHandler(this);
        jb.addActionListener(btnHandler);
        jf.add(jb);

        Vector<Vector<String>> rowData = new Vector<Vector<String>>();
        Vector<String> record = new Vector<String>();
        ResultSet rs = dbo.getOrderDetails(orderID);
        double subToatl = 0, total = 0, grandTotal;
        while (rs.next()) {
            record = new Vector<String>();
            record.add(rs.getString("Item No"));
            record.add(rs.getString("Item Name"));
            record.add(rs.getString("Quantity"));
            record.add(rs.getString("Price"));
            total = (Double.parseDouble(rs.getString("Quantity")) * Double.parseDouble(rs.getString("Price").substring(0, rs.getString("Price").length() - 2)));
            subToatl = total + subToatl;
            record.add(total + "/-");
            rowData.add(record);
        }

        area = new JTextArea();
        String upperPart = "\t\tRESTAURENT NAME\n"
                + "\tKatchery Road, Near Anarkali Bazar, Lahore, 54000\n"
                + "\t\t(+92)323 0496310\n\n"
                + "     *********************************************************************************************     \n\n";
        area.setText(upperPart);
        for (int i = 0; i < rowData.size(); i++) {
            area.setText(area.getText() + "      ");
            for (int j = 0; j < record.size(); j++) {
                area.setText(area.getText() + rowData.elementAt(i).elementAt(j) + "\t");
            }
            area.setText(area.getText() + "\n");
        }
        grandTotal = subToatl + (subToatl * 0.16);
        String midPart = "     --------------------------------------------------------------------------------------------------------------------     \n\n"
                + "\t\t\tSub-Total :  \t" + subToatl + "/-\n"
                + "\t\t\tGST :        \t16%\n"
                + "\t\t\tGrand Total :\t" + grandTotal + "/-\n\n"
                + "\t\t\tDate/Time :  \t%tD\n\n";

        area.setText(area.getText() + midPart);
        String bottomPart = "\t***************************************************************\n\n"
                + "\t\tThank You For Coming!\n\n";
        area.setText(area.getText() + bottomPart);
        area.setBounds(20, 20, 200, 300);
        area.setSize(area.getPreferredSize());
        jp.add(area);

    }
    
    private void connect() {

        try {
            server = (RestaurentServer) Naming.lookup("rmi://localhost:1099/restaurentService");
        } catch (Exception e) {

            System.out.println(e);
        }
    }

}
