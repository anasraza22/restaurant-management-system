import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author OFFICE
 */
public class RestaurentServerImpl extends UnicastRemoteObject implements RestaurentServer {

    public RestaurentServerImpl() throws RemoteException{
        System.out.println("Running Server...");
    }

    @Override
    public void sendOrderToCashier(int orderID) throws RemoteException {
        System.out.println("In sendOrderToCashier( " + orderID + " )");
    }

}
