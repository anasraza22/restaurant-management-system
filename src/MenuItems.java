
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author OFFICE
 */
public class MenuItems {
    private int itemsNo;
    private String name;
    private String qntity;
    private String price;

    public MenuItems() {
    }

    public MenuItems(int itemsNo, String name, String qntity, String price) {
        this.itemsNo = itemsNo;
        this.name = name;
        this.qntity = qntity;
        this.price = price;
    }
    /**
     * @return the itemsNo
     */
    public int getItemsNo() {
        return itemsNo;
    }

    /**
     * @param itemsNo the itemsNo to set
     */
    public void setItemsNo(int itemsNo) {
        this.itemsNo = itemsNo;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the qntity
     */
    public String getQntity() {
        return qntity;
    }

    /**
     * @param qntity the qntity to set
     */
    public void setQntity(String qntity) {
        this.qntity = qntity;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }
    
}
