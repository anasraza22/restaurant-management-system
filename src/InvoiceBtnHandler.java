import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author OFFICE
 */
public class InvoiceBtnHandler implements ActionListener{
    Invoice invoice;

    public InvoiceBtnHandler(Invoice order) {
        this.invoice = order;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == invoice.edit){
            invoice.jf.dispose();
        }
        if(e.getSource() == invoice.confrm){
            try {
                invoice.jf.dispose();
                invoice.diposeMenu();
                new Reciept(invoice.orderId);
            } catch (Exception ex) {
                Logger.getLogger(InvoiceBtnHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
